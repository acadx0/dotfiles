local function colors_monotone()
    -- local nvim_set_hl = vim.api.nvim_set_hl
    -- nvim_set_hl(0, "TelescopeNormal", { bg = "#1c1c1e" })
    -- nvim_set_hl(0, "Normal", {})
    -- nvim_set_hl(0, "Comment", { foreground = 6708828, italic = true })
    -- nvim_set_hl(0, "ColorColumn", { background = 6708828 })
    -- nvim_set_hl(0, "Conceal", {})
    -- nvim_set_hl(0, "Cursor", { background = 16145237 })
    -- nvim_set_hl(0, "CursorI", { background = 16145237 })
    -- nvim_set_hl(0, "CursorR", { background = 14985831 })
    -- nvim_set_hl(0, "CursorO", { background = 7712471 })
    -- nvim_set_hl(0, "CursorLine", { background = 855052 })
    -- nvim_set_hl(0, "DiffAdd", { foreground = 10929543 })
    -- nvim_set_hl(0, "DiffChange", { foreground = 14985831 })
    -- nvim_set_hl(0, "DiffDelete", { foreground = 16145237 })
    -- nvim_set_hl(0, "DiffText", { background = 16145237, foreground = 0 })
    -- nvim_set_hl(0, "EndOfBuffer", { foreground = 6579380 })
    -- nvim_set_hl(0, "VertSplit", { foreground = 6708828 })
    -- nvim_set_hl(0, "Folded", { background = 2630948, foreground = 9603463, italic = true })
    -- nvim_set_hl(0, "FoldColumn", {})
    -- nvim_set_hl(0, "SignColumn", {})
    -- nvim_set_hl(0, "LineNr", { foreground = "#333333" })
    -- nvim_set_hl(0, "CursorLineNr", { foreground = "#606060" })
    -- nvim_set_hl(0, "NonText", { foreground = 11293568 })
    -- nvim_set_hl(0, "NormalFloat", { background = 2630948, foreground = 10393236 })
    -- nvim_set_hl(0, "NormalNC", {})
    -- nvim_set_hl(0, "QuickFixLine", {})
    -- nvim_set_hl(0, "StatusLine", { background = "#141414", foreground = "#4c5265", italic = true })
    -- nvim_set_hl(0, "StatusLineNC", { background = "#141414", foreground = "#4c5265", italic = true })
    -- nvim_set_hl(0, "WinBar", { background = "#141414", foreground = "#4c5265", italic = true })
    -- nvim_set_hl(0, "WinBarNC", { background = "#141414", foreground = "#4c5265", italic = true })
    -- nvim_set_hl(0, "TabLine", { foreground = 9603463 })
    -- nvim_set_hl(0, "TabLineFill", { foreground = 9603463 })
    -- nvim_set_hl(0, "TabLineSel", { bold = true, foreground = 12762812 })
    -- nvim_set_hl(0, "Title", { bold = true })
    -- nvim_set_hl(0, "Visual", { background = 16777215, foreground = 0 })
    -- nvim_set_hl(0, "Whitespace", { foreground = 4933188 })
    -- nvim_set_hl(0, "Constant", { special = 16777215 })
    -- nvim_set_hl(0, "String", { foreground = 10393236 })
    -- nvim_set_hl(0, "Boolean", { italic = true })
    -- nvim_set_hl(0, "Identifier", { italic = true })
    -- nvim_set_hl(0, "Function", { bold = true })
    -- nvim_set_hl(0, "Statement", { bold = true, italic = true })
    -- nvim_set_hl(0, "Include", { italic = true })
    -- nvim_set_hl(0, "Type", { bold = true })
    -- nvim_set_hl(0, "StorageClass", {})
    -- nvim_set_hl(0, "Structure", {})
    -- nvim_set_hl(0, "Delimiter", { foreground = 12762812 })
    -- nvim_set_hl(0, "Underlined", { underline = true })
    -- nvim_set_hl(0, "Bold", { bold = true })
    -- nvim_set_hl(0, "Italic", { italic = true })
    -- nvim_set_hl(0, "Todo", { bold = true, foreground = 14985831, italic = true })
    --
    -- nvim_set_hl(0, "TSConstant", { special = 16777215, underline = true })
    -- nvim_set_hl(0, "TSProperty", { italic = true })
    -- nvim_set_hl(0, "TSStringRegex", { foreground = 12762812, italic = true })
    -- nvim_set_hl(0, "TSStringEscape", { bold = true, foreground = 16777215 })
    -- nvim_set_hl(0, "Typedef", {})
    -- nvim_set_hl(0, "Special", {})
    --
    -- nvim_set_hl(0, "Character", {})
    -- nvim_set_hl(0, "Number", {})
    -- nvim_set_hl(0, "Float", {})
    --
    -- nvim_set_hl(0, "Conditional", {})
    -- nvim_set_hl(0, "Repeat", {})
    -- nvim_set_hl(0, "Label", {})
    -- nvim_set_hl(0, "Operator", {})
    -- nvim_set_hl(0, "Keyword", {})
    -- nvim_set_hl(0, "Exception", {})
    -- nvim_set_hl(0, "PreProc", {})
    -- nvim_set_hl(0, "Define", {})
    -- nvim_set_hl(0, "Macro", {})
    -- nvim_set_hl(0, "PreCondit", {})
    -- nvim_set_hl(0, "Tag", {})
    -- nvim_set_hl(0, "Debug", {})
    --
    -- vim.defer_fn(function()
    --     nvim_set_hl(0, "ModeMsg", {})
    --     nvim_set_hl(0, "MsgArea", {})
    --     nvim_set_hl(0, "MsgSeparator", {})
    --     nvim_set_hl(0, "SpecialChar", {})
    --
    --     nvim_set_hl(0, "Search", { background = 14985831, foreground = 0 })
    --     nvim_set_hl(0, "Question", {})
    --     nvim_set_hl(0, "Substitute", { background = 14985831, foreground = 0 })
    --     nvim_set_hl(0, "MoreMsg", { bold = true, foreground = 7712471 })
    --     nvim_set_hl(0, "WildMenu", { foreground = 16777215 })
    --     nvim_set_hl(0, "VisualNOS", {})
    --     nvim_set_hl(0, "SpecialKey", { bold = true })
    --
    --     -- gitsigns.nvim
    --     nvim_set_hl(0, "GitSignsAdd", { bg = "NONE", fg = "green" })
    --     nvim_set_hl(0, "GitSignsChange", { bg = "NONE", fg = "blue" })
    --     nvim_set_hl(0, "GitSignsDelete", { bg = "NONE", fg = "red" })
    --     nvim_set_hl(0, "GitSignsChange", { bg = "NONE", fg = "yellow" })
    --
    --     -- nvim-cmp
    --     -- nvim_set_hl(0, "CmpItemAbbrDeprecated", { bg = "NONE", strikethrough = true, fg = "#808080" })
    --     -- nvim_set_hl(0, "CmpItemAbbrMatch", { bg = "NONE", fg = "#569CD6" })
    --     -- nvim_set_hl(0, "CmpItemAbbrMatchFuzzy", { bg = "NONE", fg = "#569CD6" })
    --     -- nvim_set_hl(0, "CmpItemKindVariable", { bg = "NONE", fg = "#9CDCFE" })
    --     -- nvim_set_hl(0, "CmpItemKindInterface", { bg = "NONE", fg = "#9CDCFE" })
    --     -- nvim_set_hl(0, "CmpItemKindText", { bg = "NONE", fg = "#9CDCFE" })
    --     -- nvim_set_hl(0, "CmpItemKindFunction", { bg = "NONE", fg = "#C586C0" })
    --     -- nvim_set_hl(0, "CmpItemKindMethod", { bg = "NONE", fg = "#C586C0" })
    --     -- nvim_set_hl(0, "CmpItemKindKeyword", { bg = "NONE", fg = "#D4D4D4" })
    --     -- nvim_set_hl(0, "CmpItemKindProperty", { bg = "NONE", fg = "#D4D4D4" })
    --     -- nvim_set_hl(0, "CmpItemKindUnit", { bg = "NONE", fg = "#D4D4D4" })
    --     nvim_set_hl(0, "SpecialComment", {})
    --     nvim_set_hl(0, "IncSearch", { background = 7712471, foreground = 0 })
    --     -- nvim_set_hl(0, "CurSearch", { background = "red", foreground = 0 })
    --
    --     nvim_set_hl(0, "Error", { bold = false, foreground = 16145237 })
    --     nvim_set_hl(0, "ErrorMsg", { background = 16145237, bold = false, foreground = 0 })
    --     nvim_set_hl(0, "Warning", { bold = false, foreground = 14985831 })
    --     nvim_set_hl(0, "WarningMsg", { bold = false, foreground = 14985831 })
    --
    --     nvim_set_hl(0, "SpellBad", { special = 16145237, undercurl = true })
    --     nvim_set_hl(0, "SpellCap", { special = 7712471, undercurl = true })
    --     nvim_set_hl(0, "SpellLocal", { special = 10929543, undercurl = true })
    --     nvim_set_hl(0, "SpellRare", { special = 14985831, undercurl = true })
    --
    --     nvim_set_hl(0, "SyntaxError", { special = 16145237, undercurl = true })
    --     nvim_set_hl(0, "SyntaxWarning", { special = 14985831, undercurl = true })
    --     nvim_set_hl(0, "SyntaxInfo", { special = 7712471, undercurl = true })
    --     nvim_set_hl(0, "SyntaxHint", { special = 10929543, undercurl = true })
    --
    --     nvim_set_hl(0, "Pmenu", { background = 3551792, foreground = 12762812 })
    --     nvim_set_hl(0, "PmenuSel", { background = 16777215, foreground = 0 })
    --     nvim_set_hl(0, "PmenuSbar", { background = 3551792 })
    --     nvim_set_hl(0, "PmenuThumb", { background = 9603463 })
    --
    --     nvim_set_hl(0, "MatchParen", { background = 4457988, bold = true, foreground = 16145237, italic = true })
    --     nvim_set_hl(0, "ParenMatch", { background = 4457988, bold = true, foreground = 16145237, italic = true })
    --     nvim_set_hl(0, "Directory", {})
    --
    --     nvim_set_hl(0, "LspReferenceText", { special = 7712471, underline = true })
    --     nvim_set_hl(0, "LspReferenceRead", { special = 16777215, underline = true })
    --     nvim_set_hl(0, "LspReferenceWrite", { special = 10929543, underline = true })
    --     nvim_set_hl(0, "IndentBlanklineChar", { foreground = 3551792 })
    --     nvim_set_hl(0, "IndentBlanklineContextChar", { foreground = 11293568 })
    --     -- nvim_set_hl(0, "DiagnosticError", { background = 4457988, foreground = 16145237 })
    --     nvim_set_hl(0, "DiagnosticWarn", { background = 4007179, foreground = 14985831 })
    --     nvim_set_hl(0, "DiagnosticInfo", { background = 1058615, foreground = 7712471 })
    --     nvim_set_hl(0, "DiagnosticHint", { background = 2371607, foreground = 10929543 })
    --     nvim_set_hl(0, "DiagnosticUnderlineError", { special = 16145237, undercurl = true })
    --     nvim_set_hl(0, "DiagnosticUnderlineWarn", { special = 14985831, undercurl = true })
    --     nvim_set_hl(0, "DiagnosticUnderlineInfo", { special = 7712471, undercurl = true })
    --     nvim_set_hl(0, "DiagnosticUnderlineHint", { special = 10929543, undercurl = true })
    --     nvim_set_hl(0, "DiagnosticSignError", { background = 3146755, foreground = 16145237 })
    --     nvim_set_hl(0, "DiagnosticSignWarning", { background = 2824968, foreground = 14985831 })
    --     nvim_set_hl(0, "DiagnosticSignInformation", { background = 728104, foreground = 7712471 })
    --     nvim_set_hl(0, "DiagnosticSignHint", { background = 1712657, foreground = 10929543 })
    --     nvim_set_hl(0, "DiagnosticVirtualTextError", { italic = true, fg = "#65737e" })
    --     nvim_set_hl(0, "DiagnosticVirtualTextWarn", { italic = true, fg = "#65737e" })
    --     nvim_set_hl(0, "DiagnosticVirtualTextInfo", { italic = true, fg = "#65737e" })
    --     nvim_set_hl(0, "DiagnosticVirtualTextHint", { italic = true, fg = "#65737e" })
    -- end, 100)
end

