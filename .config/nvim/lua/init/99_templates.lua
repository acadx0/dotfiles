nvim_create_autocmd("BufNewFile", {
    pattern = { "main.go" },
    command = [[ 
        execute "0r! ~/.config/nvim/templates/go-main.sh"
        normal 2k
    ]],
})
