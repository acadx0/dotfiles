-- " let g:operator_sandwich_no_default_key_mappings = 1
-- "
-- " packadd vim-sandwich
-- " runtime macros/sandwich/keymap/surround.vim
-- "
-- " let g:sandwich#recipes = deepcopy(g:sandwich#default_recipes)
-- " let g:sandwich#recipes += [
-- "       \   {
-- "       \     'buns'    : ['print(', ')'],
-- "       \     'filetype': ['python'],
-- "       \     'nesting' : 0,
-- "       \     'input'   : ['p', 'P'],
-- "       \   },
-- "       \   {
-- "       \     'buns'    : ['fmt.Printf(', ')'],
-- "       \     'filetype': ['go'],
-- "       \     'nesting' : 0,
-- "       \     'input'   : ['p', 'P'],
-- "       \   },
-- "       \   {
-- "       \     'buns'    : ['log.Printf("%#+v\n", ', ')'],
-- "       \     'filetype': ['go'],
-- "       \     'nesting' : 0,
-- "       \     'input'   : ['l', 'L'],
-- "       \   },
-- "       \   {
-- "       \     'buns'    : ['`', '`'],
-- "       \     'filetype': ['markdown'],
-- "       \     'nesting' : 0,
-- "       \     'input'   : ['c'],
-- "       \   },
-- "       \   {
-- "       \     'buns'    : ['```
-- "       \', '
-- "       \```
-- "       \'],
-- "       \     'filetype': ['markdown'],
-- "       \     'nesting' : 0,
-- "       \     'motionwise': ['line'],
-- "       \     'input'   : ['c'],
-- "       \   },
-- "       \   {
-- "       \     'buns'    : ['```
-- "       \', '
-- " \```
-- "       \'],
-- "       \     'filetype': ['markdown'],
-- "       \     'nesting' : 0,
-- "       \     'input'   : ['C'],
-- "       \   },
-- "       \   {
-- "       \     'buns'    : ['[](', ')'],
-- "       \     'filetype': ['markdown'],
-- "       \     'nesting' : 0,
-- "       \     'input'   : ['l','L'],
-- "       \   },
-- "       \   {
-- "       \     'buns'    : ['console.log(', ')'],
-- "       \     'filetype': ['javascript','typescript'],
-- "       \     'nesting' : 0,
-- "       \     'input'   : ['p', 'P'],
-- "       \   },
-- "       \   {
-- "       \     'buns'    : ['print(', ')'],
-- "       \     'filetype': ['lua'],
-- "       \     'nesting' : 0,
-- "       \     'input'   : ['p', 'P'],
-- "       \   },
-- "       \ ]

vim.cmd.packadd("mini.nvim")
require("mini.surround").setup({
    -- respect_selection_type = false,
    custom_surroundings = {
        ["("] = { input = { "%b()", "^.%s*().-()%s*.$" }, output = { left = "(", right = ")" } },
        [")"] = { input = { "%b()", "^.().*().$" }, output = { left = "(", right = ")" } },
        ["["] = { input = { "%b[]", "^.%s*().-()%s*.$" }, output = { left = "[", right = "]" } },
        ["]"] = { input = { "%b[]", "^.().*().$" }, output = { left = "[", right = "]" } },
        ["{"] = { input = { "%b{}", "^.%s*().-()%s*.$" }, output = { left = "{", right = "}" } },
        ["}"] = { input = { "%b{}", "^.().*().$" }, output = { left = "{", right = "}" } },
        ["<"] = { input = { "%b<>", "^.%s*().-()%s*.$" }, output = { left = "<", right = ">" } },
        [">"] = { input = { "%b<>", "^.().*().$" }, output = { left = "<", right = ">" } },
        ["l"] = {
            output = function(a)
                if vim.bo.filetype == "go" then
                    return { left = "log.Print(", right = ")" }
                elseif vim.bo.filetype == "typescript" then
                    return { left = "console.log(", right = ")" }
                end
            end,
        },
        ["p"] = {
            output = function(a)
                if vim.bo.filetype == "go" then
                    return { left = "fmt.Printf(", right = ")" }
                elseif vim.bo.filetype == "python" then
                    return { left = "print(", right = ")" }
                elseif vim.bo.filetype == "typescript" then
                    return { left = "console.log(", right = ")" }
                end
            end,
        },

        ["c"] = {
            -- https://github.com/echasnovski/mini.nvim/discussions/462
            output = function()
                -- is_visual doesn't work
                -- local is_visual = vim.tbl_contains({ 'v', 'V', '\22' }, vim.fn.mode())
                -- local mark_from = is_visual and "'<" or "'["
                -- local mark_to = is_visual and "'>" or "']"
                local mark_from = "'<"
                local mark_to = "'>"
                local startline = vim.fn.line(mark_from)
                local endline = vim.fn.line(mark_to)
                local n_content_lines = endline - startline
                if n_content_lines == 0 then
                    return { left = "`", right = "`" }
                end
                local firstline = vim.api.nvim_buf_get_lines(0, startline - 1, startline, false)
                local _, indent = string.find(firstline[1], '^%s*')
                if indent ~= nil then
                    return { left = "```\n" .. string.rep(" ", indent) , right = "\n" .. string.rep(" ", indent) .. "```\n" }
                else
                    return { left = "```\n", right = "\n```\n" }
                end
            end,
        },
        ["C"] = {
            output = function(a)
                return { left = "```", right = "```" }
            end,
        },
    },

    mappings = {
        add = "sa",
        delete = "sd",
        find = "sf",
        find_left = "sF",
        highlight = "sh",
        replace = "sr",
        update_n_lines = "",

        -- Add this only if you don't want to use extended mappings
        suffix_last = "",
        suffix_next = "",
    },
    search_method = "cover_or_next",
})

-- Remap adding surrounding to Visual mode selection
-- vim.keymap.del("x", "ys")
vim.keymap.set("x", "S", [[:<C-u>lua MiniSurround.add('visual')<CR>]], { silent = true })

-- Make special mapping for "add surrounding for line"
-- vim.keymap.set("n", "yss", "ys_", { remap = true })
