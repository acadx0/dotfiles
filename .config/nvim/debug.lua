vim.cmd.packadd 'nvim-cmp'
vim.cmd.packadd 'LuaSnip'
vim.cmd.packadd 'cmp_luasnip'
vim.cmd.packadd 'cmp-buffer'

local cmp = require("cmp")
local luasnip = require("luasnip")
local ls = luasnip

-- cmp.setup({
--     snippet = {
--         -- REQUIRED - you must specify a snippet engine
--         expand = function(args)
--             -- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
--             require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
--             -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
--             -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
--         end,
--     },
--     window = {
--         -- completion = cmp.config.window.bordered(),
--         -- documentation = cmp.config.window.bordered(),
--     },
--     mapping = cmp.mapping.preset.insert({
--         ['<C-b>'] = cmp.mapping.scroll_docs(-4),
--         ['<C-f>'] = cmp.mapping.scroll_docs(4),
--         ['<C-Space>'] = cmp.mapping.complete(),
--         ['<C-e>'] = cmp.mapping.abort(),
--         ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
--     }),
--     sources = cmp.config.sources({
--         { name = 'nvim_lsp' },
--         { name = 'vsnip' }, -- For vsnip users.
--         { name = 'luasnip' }, -- For luasnip users.
--         -- { name = 'ultisnips' }, -- For ultisnips users.
--         -- { name = 'snippy' }, -- For snippy users.
--     }, {
--         { name = 'buffer' },
--     })
-- })

cmp.setup({
    -- documentation = { -- no border; native-style scrollbar
    --   -- border = nil,
    --   -- scrollbar = '',
    --   -- other options
    -- },
    -- window = {
    --     completion = cmp.config.window.bordered(),
    --     documentation = cmp.config.window.bordered(),
    -- },

    experimental = {
        ghost_text = true,
    },
    -- confirmation = {
    --     get_commit_characters = function()
    --         return {}
    --     end,
    -- },

    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },

    -- sorting = {
    --     comparators = {
    --         cmp.config.compare.offset,
    --         cmp.config.compare.exact,
    --         cmp.config.compare.score,
    --         require("cmp-under-comparator").under,
    --         cmp.config.compare.kind,
    --         cmp.config.compare.sort_text,
    --         cmp.config.compare.length,
    --         cmp.config.compare.order,
    --     },
    -- },

    -- formatting = {
    --     -- format = function(_, vim_item)
    --     --     vim_item.kind = (cmp_kinds[vim_item.kind] or "") .. vim_item.kind
    --     --     return vim_item
    --     -- end,
    --     fields = { "kind", "abbr", "menu" },
    --     format = function(_, vim_item)
    --         vim_item.menu = vim_item.kind
    --         vim_item.kind = cmp_kinds[vim_item.kind]
    --
    --         return vim_item
    --     end,
    -- },

    preselect = "none",
    mapping = {
        ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
        -- ["<CR>"] = cmp.mapping(function(fallback)
        --
        --     if cmp.core.view:get_selected_entry() then
        --         cmp.confirm({
        --             behavior = cmp.ConfirmBehavior.Replace,
        --             select = true,
        --         })
        --     else
        --         fallback()
        --     end
        -- end, {
        --     "i",
        --     "s",
        -- }),
        ["<c-e>"] = cmp.mapping(function(fallback)
            -- if luasnip.expandable() then
            --     require("luasnip").expand()
            -- else
            --     fallback()
            -- end
        end, {
            "i",
        }),
        ["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
        ["<C-p>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
        ["<Down>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
        ["<Up>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
        ["<Tab>"] = cmp.mapping(function(fallback)
            -- local next_char = vim.api.nvim_eval("strcharpart(getline('.')[col('.') - 1:], 0, 1)")
            if cmp.visible() then
                cmp.select_next_item()
            -- elseif luasnip.jumpable(1) then
            --     luasnip.jump(1)
                -- luasnip.unlink_current()
                -- elseif
                --     next_char == '"'
                --     or next_char == ")"
                --     or next_char == "'"
                --     or next_char == "]"
                --     or next_char == "}"
                --     or next_char == "("
                -- then
                --     vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<Right>", true, true, true), "n", true)
            else
                fallback()
            end
        end, {
            "i",
            "s",
            "n",
        }),
        ["<S-Tab>"] = function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            -- elseif luasnip.jumpable(-1) then
            --     luasnip.jump(-1)
            -- else
            --     fallback()
            end
        end,
    },
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'vsnip' }, -- For vsnip users.
        { name = 'luasnip' }, -- For luasnip users.
        -- { name = 'ultisnips' }, -- For ultisnips users.
        -- { name = 'snippy' }, -- For snippy users.
    }, {
        { name = 'buffer' },
    })
})

local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local l = require("luasnip.extras").lambda
local rep = require("luasnip.extras").rep
local p = require("luasnip.extras").partial
local m = require("luasnip.extras").match
local n = require("luasnip.extras").nonempty
local dl = require("luasnip.extras").dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local types = require("luasnip.util.types")
local conds = require("luasnip.extras.conditions")
local conds_expand = require("luasnip.extras.conditions.expand")

ls.add_snippets("all", {
	-- trigger is `fn`, second argument to snippet-constructor are the nodes to insert into the buffer on expansion.
	s("fn", {
		t("functions"),
	}),
})
